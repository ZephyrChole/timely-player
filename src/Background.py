#!/usr/bin/python
# -*- coding: UTF-8 -*-

# @author:ZephyrChole
# @file:Background.py
# @time:2021/08/18
import os
import pickle
import threading
import time
from threading import Thread
from typing import List
from pygame import mixer
from moviepy.editor import AudioFileClip


class Music:
    outputMaxLength = 15

    def __init__(self, path):
        self.name = os.path.split(path)[1]
        self.path = path
        # this way is quite slow.
        self.duration = AudioFileClip(path).duration

        # this way is abandoned because it need ffmpeg to be environment variable.
        # self.duration = librosa.get_duration(filename=path)

    def get_name_str(self):
        if len(self.name) > self.outputMaxLength:
            return f'{self.name[:self.outputMaxLength]}...'
        else:
            return self.name


class Event:
    outputMaxLength = 15
    delay = 0

    def __init__(self, hour, minute, comment, music: Music):
        self.hour = hour
        self.minute = minute
        self.comment = comment
        self.music = music

    def play(self):
        mixer.music.load(self.music.path)
        mixer.music.play()
        return time.time()

    def get_time_str(self):
        now_tuple = tuple(time.localtime(time.time()))
        target = time.mktime(time.struct_time(now_tuple[:3] + (self.hour, self.minute, 0) + now_tuple[6:]))
        return time.strftime('%H:%M', time.localtime(target))

    def get_comment_str(self):
        return f'{self.comment[:self.outputMaxLength]}...' if len(self.comment) > self.outputMaxLength else self.comment


class MyEvents:
    events: List[Event] = []

    def __init__(self, settings_path):
        self.settings_path = settings_path
        self.load()

    def load(self):
        try:
            with open(self.settings_path, 'rb') as file:
                self.events = pickle.loads(file.read())
        except EOFError:
            pass
        except FileNotFoundError:
            open(self.settings_path, 'w').close()

    def add(self, hour, minute, comment, musicPath):
        event = Event(hour, minute, comment, Music(musicPath))
        self.events.append(event)

    def sort(self):
        self.events.sort(key=lambda event: event.hour * 3600 + event.minute * 60)

    def save(self):
        with open(self.settings_path, 'wb') as file:
            file.write(pickle.dumps(self.events))

    def remove(self, event):
        self.events.remove(event)

    def refresh_delay(self):
        for i in range(len(self.events) - 1):
            cur_event = self.events[i]
            next_event = self.events[i + 1]
            result = (cur_event.hour - next_event.hour) * 3600 + (
                    cur_event.minute - next_event.minute) * 60 + cur_event.music.duration + cur_event.delay
            if result > 0:
                next_event.delay = result

    def get_collision(self):
        self.refresh_delay()
        collision = []
        for i in range(len(self.events)):
            cur_event = self.events[i]
            if cur_event.delay > 0:
                last_event = self.events[i - 1]
                collision.append(
                    f'{cur_event.music.get_name_str()} 将被 {last_event.music.get_name_str()} 推迟 {int(cur_event.delay)}秒')
        return collision


class Background(Thread):
    done = False
    _stop_event = threading.Event()

    def __init__(self, pickle_path):
        super().__init__()
        self.eventManager = MyEvents(pickle_path)
        mixer.init()

    def run(self):
        def check_come():
            now = time.time()
            now_tuple = tuple(time.localtime(now))
            today = time.mktime(time.struct_time(now_tuple[:3] + (0, 0, 0) + now_tuple[6:]))
            target = e.hour * 3600 + e.minute * 60 + today + e.delay
            return target > now and target < now + 5

        while not self.is_stopped():
            for e in self.eventManager.events:
                if check_come():
                    e.play()
                    break

            time.sleep(5)

    def stop(self):
        self._stop_event.set()

    def is_stopped(self):
        return self._stop_event.is_set()
