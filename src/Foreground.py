#!/usr/bin/python
# -*- coding: UTF-8 -*-

# @author:ZephyrChole
# @file:Foreground.py
# @time:2021/08/18
from typing import List

from PyQt5 import sip
from PyQt5.QtCore import Qt, QTime
from PyQt5.QtGui import QResizeEvent
from PyQt5.QtWidgets import QVBoxLayout, QGridLayout, QLabel, QPushButton, QHBoxLayout, QGroupBox, QWidget, \
    QDesktopWidget, QFileDialog, QTimeEdit, QLineEdit, QMessageBox

from src.Background import Background


class Info:
    rowMaxLength = 20

    def __init__(self, comment):
        self.comment = comment


class Foreground(QWidget):
    infos: List[Info] = []
    box_to_event = {}
    height = 600
    width = 800
    row = 6
    column = 10
    row_length = height / row
    column_length = width / column
    left_span = 6
    right_span = column - left_span

    def __init__(self, background: Background):
        super().__init__()
        self.background = background
        self.initUI()
        self.show()

    def initUI(self):
        self.mainLayout = QVBoxLayout()
        self.topLayout = QGridLayout()
        taskLabel = QLabel('定时任务')
        taskLabel.setAlignment(Qt.AlignCenter)
        taskLabel.setStyleSheet('background-color: RoyalBlue;font-size:15px;color:rgb(255,255,255)')
        self.topLayout.addWidget(taskLabel, 0, 0, 1, self.left_span)
        infoLabel = QLabel('信息')
        infoLabel.setAlignment(Qt.AlignCenter)
        infoLabel.setStyleSheet('background-color: Red;font-size:15px;color:rgb(255, 255, 255)')
        self.topLayout.addWidget(infoLabel, 0, self.left_span, 1, self.right_span - 1)
        addButton = QPushButton('+', self)
        addButton.clicked.connect(self.addEvent)
        self.topLayout.addWidget(addButton, 0, self.column - 1)
        self.mainLayout.addLayout(self.topLayout, 1)

        self.bottomlayout = QHBoxLayout()

        self.taskLayout = QVBoxLayout()
        self.taskLayout.setAlignment(Qt.AlignTop)
        self.taskBox = QGroupBox()
        self.taskBox.setLayout(self.taskLayout)
        self.bottomlayout.addWidget(self.taskBox, self.left_span)
        self.refresh_taskLayout()

        self.infoLayout = QVBoxLayout()
        self.infoLayout.setAlignment(Qt.AlignTop)
        self.infoBox = QGroupBox()
        self.infoBox.setLayout(self.infoLayout)
        self.bottomlayout.addWidget(self.infoBox, self.right_span)
        self.refresh_infoLayout()

        self.mainLayout.addLayout(self.bottomlayout, self.row - 1)
        self.setLayout(self.mainLayout)
        self.resize(self.width, self.height)
        self.center()
        self.setWindowTitle('定时播放')

    def refresh_taskLayout(self):
        for i in range(self.taskLayout.count()):
            wd = self.taskLayout.itemAt(0).widget()
            self.taskLayout.removeWidget(wd)
            sip.delete(wd)
        self.box_to_event = {}

        for event in self.background.eventManager.events:
            newLayout = QHBoxLayout()
            timeLabel = QLabel(event.get_time_str())
            timeLabel.setStyleSheet('font-size:15px;color:rgb(142,57,52)')
            newLayout.addWidget(timeLabel, 3)
            musicLabel = QLabel(event.music.get_name_str())
            musicLabel.setToolTip(event.music.name)
            musicLabel.setStyleSheet('font-size:15px;color:rgb(44,28,31)')
            newLayout.addWidget(musicLabel, 4)
            commentLabel = QLabel(event.get_comment_str())
            commentLabel.setToolTip(event.comment)
            commentLabel.setStyleSheet('font-size:15px;color:rgb(83,123,135)')
            newLayout.addWidget(commentLabel, 4)
            delButton = QPushButton('删除')
            delButton.clicked.connect(self.delEvent)
            newLayout.addWidget(delButton, 1)
            newBox = QGroupBox()
            newBox.setLayout(newLayout)
            self.taskLayout.addWidget(newBox)
            newBox.setMaximumSize(self.column_length * self.left_span, self.row_length)
            self.box_to_event[id(newBox)] = event
        self.refresh_collision()

    def refresh_collision(self):
        self.infos = [Info(c) for c in self.background.eventManager.get_collision()]

    def delEvent(self):
        p = self.sender().parent()
        self.taskLayout.removeWidget(p)
        sip.delete(p)
        self.background.eventManager.remove(self.box_to_event[id(p)])
        self.refresh_collision()
        self.refresh_infoLayout()

    def refresh_infoLayout(self):
        for i in range(self.infoLayout.count()):
            wd = self.infoLayout.itemAt(0).widget()
            self.infoLayout.removeWidget(wd)
            sip.delete(wd)

        for info in self.infos:
            newLabel = QLabel(f'{info.comment}')
            newLabel.setStyleSheet(f'background-color: rgb(255, 102, 102);font-size:15px;')
            newLabel.setAlignment(Qt.AlignCenter)
            newLabel.setWordWrap(True)
            newLabel.setMaximumSize(self.column_length * self.right_span, self.row_length * 2)
            self.infoLayout.addWidget(newLabel)

    def center(self):
        # 获得窗口
        qRect = self.frameGeometry()
        # 获得屏幕中心点
        centerPoint = QDesktopWidget().availableGeometry().center()
        # 显示到屏幕中心
        qRect.moveCenter(centerPoint)
        self.move(qRect.topLeft())

    def addEvent(self):
        def saveInput():
            hour = qTimeEdit.time().hour()
            minute = qTimeEdit.time().minute()
            comment = commentLineEdit.text()
            self.background.eventManager.add(hour, minute, comment, path[0])
            delInputBox()
            self.refresh_taskLayout()
            self.refresh_infoLayout()

        def delInputBox():
            self.mainLayout.removeWidget(self.inputBox)
            sip.delete(self.inputBox)

        def get_file():
            allowed_extensions = ['mp3', 'wma', 'ape', 'flac', 'dts', 'm4a', 'aac', 'ac3', 'mmf', 'amr', 'm4r', 'ogg',
                                  'wav', 'wavepack', 'mp2']
            file_types = list(map(lambda extension: f'*.{extension}', allowed_extensions))
            return QFileDialog.getOpenFileName(self, 'Open file', '/home', ';;'.join(file_types))

        path = get_file()
        if path[0]:
            inputLayout = QHBoxLayout(self)
            qTimeEdit = QTimeEdit(QTime.currentTime())
            inputLayout.addWidget(qTimeEdit, 3)

            commentLineEdit = QLineEdit(self)
            inputLayout.addWidget(commentLineEdit, 3)

            okButton = QPushButton('确定')
            okButton.clicked.connect(saveInput)
            inputLayout.addWidget(okButton, 1)
            self.inputBox = QGroupBox(self)
            self.inputBox.setLayout(inputLayout)
            self.mainLayout.addWidget(self.inputBox)

    def resizeEvent(self, a0: QResizeEvent) -> None:
        self.height = self.size().height()
        self.width = self.size().width()
        self.row_length = self.height / self.row
        self.column_length = self.width / self.column
        self.refresh_taskLayout()
        self.refresh_infoLayout()

    def closeEvent(self, event):
        reply = QMessageBox.question(self, '定时播放', '确定要退出吗？', QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.background.eventManager.save()
            self.background.stop()
            event.accept()
        else:
            event.ignore()

