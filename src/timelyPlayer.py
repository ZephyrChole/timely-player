# -*- coding: utf-8 -*-#

# Author:Jiawei Feng
# @software: PyCharm
# @file: timelyPlayer.py
# @time: 2021/4/30 11:21
import sys
from PyQt5.QtWidgets import QApplication

from src.Background import Background
from src.Foreground import Foreground


class TimelyPlayer:
    def __init__(self, settings_path):
        self.background = Background(settings_path)

    def main(self):
        app = QApplication(sys.argv)
        GUI = Foreground(self.background)
        GUI.show()
        self.gui = GUI
        self.background.start()
        app.exec_()
        self.background.join()


def main():
    tp = TimelyPlayer('./setting.pickle')
    tp.main()


if __name__ == '__main__':
    main()
