#!/usr/bin/python
# -*- coding: UTF-8 -*-

# @author:ZephyrChole
# @file:utils.py
# @time:2021/08/18
import os


def check_path(dirPath):
    if os.path.exists(dirPath):
        return True
    else:
        path, name = os.path.split(dirPath)
        if check_path(path):
            try:
                os.mkdir(dirPath)
                return True
            except:
                return False
        else:
            return False

def main():
    pass


if __name__ == '__main__':
    main()
